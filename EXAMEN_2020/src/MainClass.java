import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

class createInterface extends JFrame
{
	JFrame window=new JFrame();
	JButton buton=new JButton();
	static JTextArea text1=new JTextArea();
	static JTextArea text2=new JTextArea();
	createInterface()
	{
		window.setSize(400, 600);
		window.setTitle("Aplicatie Text");
		window.setDefaultCloseOperation(window.EXIT_ON_CLOSE);
		window.setVisible(true);
		init();
		window.setLayout(null);
	}
    public void init()
    {
    	text1.setBounds(30, 30, 300, 200);
    	window.add(text1);
    	text2.setBounds(30, 250, 300, 200);
    	window.add(text2);
    	buton.setBounds(90, 480, 150, 50);
    	buton.setText("Transfera text");
    	window.add(buton);
    	buton.addActionListener(new Eveniment());
    }
}
class Eveniment implements ActionListener
{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String textdepreluat=createInterface.text1.getText();///preiau textul introdus
		createInterface.text1.setText(null);                 ///sterg textul din prima casuta, asta nu e obligatoriu
		createInterface.text2.setText(textdepreluat);        ///il adaug in casuta2
	}
}
public class MainClass {

	public static void main(String[] args) {
		new createInterface();
	}

}
